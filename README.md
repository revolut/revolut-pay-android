# Revolut Pay: Android SDK

The Revolut Pay SDK for Android lets you accept Revolut Pay payments in your app through a fast and straightforward integration.

## Get started
Integration guides are available on [Revolut Developer](https://developer.revolut.com/docs/guides/accept-payments/payment-methods/revolut-pay/mobile/android/revolut-pay-android).


The [Demo project](demo) allows you to test out the SDK and better understand how to implement it.